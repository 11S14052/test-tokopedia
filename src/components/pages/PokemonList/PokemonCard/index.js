import _                          from 'lodash';
import PropTypes                  from 'prop-types';
import CloseIcon                  from '../../../../assets/img/close.svg';
import PokemonDeleteModal         from '../../../../components/pages/PokemonList/PokemonDeleteModal';
import OverlayBackground          from '../../../../components/common/OverlayBackground';
import { capitalizeFirstLetter }  from '../../../../helpers/methods';
import { useQuery }               from '@apollo/client';
import { Link }                   from 'react-router-dom';
import { GET_POKEMON_QUERY }      from './queries';
import { StyledPokemonCard }      from './styles';
import React, { useState }        from 'react';

const PokemonCard = (props) => {
  const { isMine, pokemon, setPokemons } = props;
  const [isModal, setIsModal] = useState(false);
  const [modalState, setModalState] = useState(-1);

  const { loading, error, data } = useQuery(GET_POKEMON_QUERY, {
    variables: { name: pokemon.name },
  });

  if (error) return `Error! ${error.message}`;

  const handleRemovePokemon = () => {
    if (isMine) {
      console.log(modalState)
      const myPokemon = JSON.parse(localStorage.getItem("myPokemon"));
      let newMyPokemon = { data: [] };

      for (let i = 0; i < myPokemon.data.length; i++) {
        if (myPokemon.data[i].nickname !== pokemon.nickname) {
          newMyPokemon.data.push(myPokemon.data[i])
        }
      }

      setPokemons(newMyPokemon.data);

      localStorage.setItem("myPokemon", JSON.stringify(newMyPokemon));
      setIsModal(false);
      setModalState(-1);
  
    }
  };

  const handleCloseModal = () => {
    setIsModal(false);
    setModalState(-1);
  };

  const showModalDelete = () => {
    setModalState();
    setIsModal(true);
  }

  const renderModal = () => {
    return (
      <PokemonDeleteModal
        handleCancel={handleCloseModal}
        handleOk={handleRemovePokemon}
      />
    );
  };

  const goToDetail = () => {
    window.location.href = isMine ? `/list-pokemon/my/${pokemon.name}/${pokemon.nickname}` : `/list-pokemon/${pokemon.name}`;
  };

  return (
    <StyledPokemonCard>
      <div className="pokemon-card--left" onClick={goToDetail}>
        <Link
          className="pokemon-card__name"
          to = {isMine ? `/list-pokemon/my/${pokemon.name}/${pokemon.nickname}` : `/list-pokemon/${pokemon.name}`}
        >
          {capitalizeFirstLetter(pokemon.name)}
          {isMine && <span>( {pokemon.nickname} )</span>}
        </Link>
        <div className="pokemon-card__types">
          {loading ? 'Loading ...' 
            : _.map(_.get(data, 'pokemon.types', []), (types, index) => (
              <div
                className="pokemon-card__type"
                key={`pokemon-card__types-${index}`}
              >
                {capitalizeFirstLetter(types.type.name)}
              </div>
            ))
          }
        </div>
      </div>
      
      <div className="pokemon-card--right">
        <img
          className="pokemon-card__avatar"
          onClick={goToDetail}
          src={isMine ? _.get(data, 'pokemon.sprites.front_default', '') : pokemon.image}
          alt={"Avatar"}
        />
        {isMine && (
          <img
            aria-hidden="true"
            className="pokemon-card__remove"
            src={CloseIcon}
            onClick={showModalDelete}
            alt="X"
          />
        )}
      </div>
      {isModal && (
        <OverlayBackground>{renderModal()}</OverlayBackground>
      )}

    </StyledPokemonCard>
  );
};

PokemonCard.propTypes = {
  isMine: PropTypes.bool,
  pokemon: PropTypes.object.isRequired,
  setPokemons: PropTypes.func.isRequired,
};

PokemonCard.defaultProps = {
  isMine: false,
}

export default PokemonCard;