import React            from 'react';
import PropTypes        from 'prop-types';
import Button           from '../../../common/Button';
import Modal            from '../../../common/Modal';

const PokemonDeleteModal = (props) => {
  const { handleCancel, handleOk } = props;

  return (
    <Modal>
      <div className="modal__title">
        Are you sure you want remove
        <br />
        your pokemon ?
      </div>
      <div className="modal__button--grouped">
        <Button handleClick={handleOk}>Yes</Button>
        <Button handleClick={handleCancel} color="red">Cancel</Button>
      </div>
    </Modal>
  );
};

PokemonDeleteModal.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  handleOk: PropTypes.func.isRequired,
};

export default PokemonDeleteModal;