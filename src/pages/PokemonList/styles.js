import styled from '@emotion/styled';

export const PokemonListWrapper = styled.div`
  padding: 24px;

  .pokemon-list {
    &__content {
      display: grid;
      row-gap: 20px;
    }

    &__count {
      font-size: 12px;
      margin-bottom: 16px;
      text-align: left;

      span {
        font-size: 20px;
        font-weight: 700;
      }
    }
    
    &__header {
      align-items: center;
      display: flex;
      margin-bottom: -10px;

      h1 {
        font-size: 18px;
        font-weight: 700;
        margin-left: -10px;
      }

      img {
        margin-right: 16px;
        width: 20px;
      }
    }

    &__search {
      align-items: center;
      display: flex;
      margin-bottom: 40px;

      img {
        width: 17px;
        cursor: pointer;
      }

      input {
        padding: 6px 10px;
        width: 90%;
        color: #666;
        font-size: 16px;
        font-weight: 500;
        margin-left: 10px;
        border-top-style: hidden;
        border-right-style: hidden;
        border-left-style: hidden;
        border-bottom-style: groove;
      }
    }

    &__icon-search {
      margin-top: -30px;
      margin-left: 300px;
      margin-bottom: 40px;
      position: relative;

      img {
        width: 20px;
        cursor: pointer;
      }
    }

    &__more {
      color: #4c4c4c;
      cursor: pointer;
      display: contents;

      img {
        width: 30px;
      }
    }

    &__tab {
      border-bottom: 2px solid #c4c4c4;
      cursor: pointer;
      flex: 1 1 auto;
      font-weight: 300;
      padding: 9px;
    }

    &__tabs {
      align-items: center;
      display: flex;
      justify-content: space-between;
      margin-bottom: 16px;

      .active {
        border-bottom: 2px solid #8792e2;
        cursor: default;
        font-weight: 500;
      }
    }
  }
`;