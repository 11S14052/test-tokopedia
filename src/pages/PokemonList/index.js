import _                                              from 'lodash';
import PokemonCard                                    from '../../components/pages/PokemonList/PokemonCard';
import Logo                                           from '../../assets/img/logo.jpg';
import Search                                         from '../../assets/img/search.png';
import Arrow                                          from '../../assets/img/arrow.png';
import ArrowUp                                        from '../../assets/img/up-arrow.png';
import React, { useEffect, useState }                 from 'react';
import { useQuery }                                   from '@apollo/client';
import { TABS }                                       from './constants';
import { GET_POKEMONS_QUERY, GET_POKEMONS_VARIABLES } from './queries';
import { PokemonListWrapper }                         from './styles';

const PokemonList = (props) => {
  const [offset, setOffset] = useState(GET_POKEMONS_VARIABLES.OFFSET);
  const [pokemons, setPokemons] = useState([]);
  const [tab, setTab] = useState(TABS.ALL);
  const [isSearch, setState] = useState(false);
  const [valueSearch, setValueSearch] = useState('');

  const { loading, error, data } = useQuery(GET_POKEMONS_QUERY, {
    variables: {
      limit: GET_POKEMONS_VARIABLES.LIMIT,
      offset,
    },
  });

  useEffect(() => {
    if (tab === TABS.ALL) {
      if (offset === 0) {
        setPokemons(_.get(data, 'pokemons.results', []));
      }
    } else {
      setOffset(0);

      const myPokemon = JSON.parse(localStorage.getItem("myPokemon"));
      if (myPokemon && myPokemon.data.length) {
        setPokemons(myPokemon.data);
      }
    }
  }, [tab]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (tab === TABS.ALL) {
      setPokemons(_.concat(pokemons, _.get(data, 'pokemons.results', [])));
    }
  }, [data]); // eslint-disable-line react-hooks/exhaustive-deps

  if (error) return `Error! ${error.message}`;

  const handleChangeTab = () => {
    if (tab === TABS.ALL) {
      setPokemons([]);
      setTab(TABS.MINE);
    } else {
      setTab(TABS.ALL);
    };
  };

  const handleClicSearch = (action) => {
    if (action === "show") {
      setState(true)
    } else {
      setState(false)
      setValueSearch('');
      defaultData();
    }
  };

  const handleClickMore = () => {
    setOffset(offset + GET_POKEMONS_VARIABLES.LIMIT);
  };

  const handleChange = (e) => {
    setValueSearch(e.target.value);
    if (e.target.value) {
      const newData = pokemons.filter(item => {
        return Object.keys(item).some(key =>
          item['name'].toLowerCase().includes(e.target.value.toLowerCase())
        );
      });
      setPokemons(newData);
    } else {
      defaultData();
    }
  }

  const defaultData = () => {
    if (tab === TABS.ALL) {
      setPokemons(_.get(data, 'pokemons.results', []));
    } else {
      const myPokemon = JSON.parse(localStorage.getItem("myPokemon"));
      if (myPokemon && myPokemon.data.length) {
        setPokemons(myPokemon.data);
      }
    }
  }

  const scrollUp = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  const totalPokemon = tab === TABS.ALL && !valueSearch ? _.get(data, 'pokemons.count', 0) : pokemons.length;

  return (
    <PokemonListWrapper>
      {
        !isSearch ? 
          <div className="pokemon-list__header">
            <img src={Logo} alt="" />
            <h1>Tokopoke</h1>
          </div>
        : null
      }
      {
        isSearch ? 
          <div className="pokemon-list__search">
            <img src={Arrow} alt="" onClick={()=>handleClicSearch("close")}/>
            <input type="text" placeholder="Search..." name="name" onChange={handleChange}></input>
          </div>
        : 
          <div className="pokemon-list__icon-search">
            <img src={Search} alt="" onClick={()=>handleClicSearch("show")}/>
          </div>
      }
      <div className="pokemon-list__tabs">
        <div
          aria-hidden="true"
          className={`pokemon-list__tab ${tab === TABS.ALL ? 'active' : ''}`}
          onClick={tab === TABS.MINE ? handleChangeTab : undefined}
        >
          All Pokemon
        </div>
        <div
          aria-hidden="true"
          className={`pokemon-list__tab ${tab === TABS.MINE ? 'active' : ''}`}
          onClick={tab === TABS.ALL ? handleChangeTab : undefined}
        >
          My Pokemon
        </div>
      </div>

      <div className="pokemon-list__count">
        <span>{totalPokemon}</span>
        &nbsp; Pokemon{totalPokemon > 1 ? 's' : ''} found
      </div>
      
      <div className="pokemon-list__content">
        {tab === TABS.ALL && loading && offset === GET_POKEMONS_VARIABLES.OFFSET ? 'Loading ...' 
          : _.map(pokemons, (pokemon, index) => (
            <PokemonCard
              isMine={tab === TABS.MINE}
              key={`pokemon-list__content-${index}`}
              pokemon={pokemon}
              setPokemons={setPokemons}
            />
          ))
        }
        {tab === TABS.ALL && _.get(data, 'pokemons.next') && (
          <div className="pokemon-list__more">
            <div
              aria-hidden="true"
              onClick={handleClickMore}
            >
              {loading && offset !== GET_POKEMONS_VARIABLES.OFFSET ? 'Loading ...' : valueSearch.length === 0 ? 'More Pokemons' : null}
            </div>
            <img src={ArrowUp} alt="Scroll Up" onClick={scrollUp} />
          </div>
        )}
      </div>
    </PokemonListWrapper>
  );
};

export default PokemonList;