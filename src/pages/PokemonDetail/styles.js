import styled         from '@emotion/styled';
import { keyframes }  from '@emotion/react'

export const bounce = keyframes`
  from, 20%, 53%, 80%, to {
    transform: translate3d(0,0,0);
  }

  40%, 43% {
    transform: translate3d(0, -30px, 0);
  }

  70% {
    transform: translate3d(0, -15px, 0);
  }

  90% {
    transform: translate3d(0,-4px,0);
  }
`;

export const PokemonDetailWrapper = styled.div`
  background: linear-gradient(to bottom right, #28e47c, #fff);
  display: flex;
  flex-direction: column;
  height: 100%;
  
  .pokemon-detail {
    &--bottom {
      background: #fff;
      flex: 1 1 auto;
      padding: 48px 24px 0 24px;
      margin-top: -20px;
    }

    &--top {
      display: flex;
      flex: none;
      flex-direction: column;
      margin-bottom: -25px;
      padding: 24px 24px 0 24px;
    }

    &__avatar {
      width: 50%;
      display: flex;
      margin-bottom: 20px;
      
      img {
        border-radius: 50%;
        margin-bottom: 200px;
        width: 40%;
      }
    }

    &__name {
      display: flex;
      margin-top: -285px;
      margin-left: 85px;
      margin-bottom: 140px;
      text-align: left;

      &__title {
        font-size: 20px;
        color: #fff;
        font-weight: bold
      }

      &__nickname {
        color: dimgray;
        font-size: 13px;
      }

      b {
        color: dimgray;
        font-size: 13px;
      }
    }

    &__button {
      cursor: pointer;
      margin-top: 30px;
    }

    &__content {
      align-items: start;
      background: #fff;
      border-radius: 5px;
      padding: 48px 24px 0 24px;
      margin-top: -90px;
      height: 50px;
      align-items: start;
      width: 50%;
      display: flex;
      margin-left: auto;
      margin-right: auto;
      box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1);

      &--line {
        border-left: 1px solid #BEBEBE;
        height: 50px;
        margin: auto;
        margin-top: -25px;
      }

      &--height {
        margin-top: -35px;

        img {
          width: 30px;
        }
      }

      &--weight {
        margin-top: -35px;
        
        img {
          width: 30px;
        }
      }
    }

    &__data {
      align-items: start;
      display: flex;
      flex: 1 20 0%;
      flex-direction: column;

      &--horizontal {
        border: 0; 
        height: 1px; 
        background-image: linear-gradient(to right, #f0f0f0, #00b9ff, #59d941, #f0f0f0);
      }

      &__label {
        color: #a1a2a6;
        font-size: 500;
        margin-bottom: 8px;
      }

      &__moves {
        color: #4c4c4c
      }

      &__type {
        align-items: center;
        border-radius: 99px;
        color: #fff;
        display: flex;
        font-size: 12px;
        font-weight: bold;
        justify-content: center;
        margin: 0 9px 9px 0;
        padding: 8px 12px;
      }

      &__moves, &__types {
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 11px;
      }

      &__type {
        background: #4fc1a6;
      }

      &__value {
        margin-bottom: 20px;
      }
    }

    &__nav {
      align-self: flex-start;
      text-decoration-line: none;      
      margin-bottom: 20px;

      img {
        width: 17px;
        cursor: pointer;
      }
      
      b {
        font-size: 18px;
        cursor: pointer;
        color: #fff;
        margin-left: 10px;
      }
    }

  }
`;