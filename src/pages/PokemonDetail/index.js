import _                                      from 'lodash';
import PropTypes                              from 'prop-types';
import Back                                   from '../../assets/img/arrow-white.png';
import Height                                 from '../../assets/img/height.png';
import Weight                                 from '../../assets/img/weight.png';
import CatchIcon                              from '../../assets/img/catch.png';
import OverlayBackground                      from '../../components/common/OverlayBackground';
import PokemonFailCatchModal                  from '../../components/pages/PokemonDetail/PokemonFailCatchModal';
import PokemonSuccessCatchModal               from '../../components/pages/PokemonDetail/PokemonSuccessCatchModal';
import { useState }                           from 'react';
import { useParams }                          from 'react-router-dom';
import { useQuery }                           from '@apollo/client';
import { css }                                from '@emotion/react'
import { capitalizeFirstLetter }              from '../../helpers/methods';
import { MODAL_OPTIONS }                      from './constants';
import { GET_POKEMON_QUERY }                  from './queries';
import { bounce, PokemonDetailWrapper }       from './styles';
import { ToastContainer, toast }              from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PokemonDetail = (props) => {
  const { isMine } = props;

  const { name, nickname: nicknameParam } = useParams();

  const [errorMessage, setErrorMessage] = useState("");
  const [isModal, setIsModal] = useState(false);
  const [modalState, setModalState] = useState(-1);
  const [nickname, setNickname] = useState(nicknameParam || "");

  const { loading, error, data } = useQuery(GET_POKEMON_QUERY, {
    variables: { name },
  });

  if (error) return `Error! ${error.message}`;

  const back = () => {
    window.location.href = `/`;
  }

  const handleChooseModal = () => {
    return Math.floor(Math.random() * Math.floor(_.size(MODAL_OPTIONS)));
  };

  const handleCatchPokemon = () => {
    setModalState(handleChooseModal());
    setIsModal(true);
  };

  const handleCloseModal = () => {
    setIsModal(false);
    setModalState(-1);
  };

  const handleRetryCatch = () => {
    handleCatchPokemon();
  };

  const handleSave = () => {
    if (nickname.length) {
      let myPokemon = localStorage.getItem("myPokemon") ? JSON.parse(localStorage.getItem("myPokemon")) : { data: [] };
      let isExist = false;

      if (myPokemon && myPokemon.data.length) {
        for (let i = 0; i < myPokemon.data.length; i++) {
          if (myPokemon.data[i].nickname === nickname) {
            isExist = true;
            break;
          }
        }
      }
      
      if (isExist) {
        setErrorMessage("Sorry, nickname already exists.");
      } else {
        myPokemon = {
          data: [...myPokemon.data, { name, nickname }]
        };
        localStorage.setItem("myPokemon", JSON.stringify(myPokemon));

        handleCloseModal();
        setErrorMessage("");
        setNickname("");
        toast( "Your pokemon's nickname has been saved." )
      }
    } else {
      setErrorMessage("Sorry, nickname is required.")
    }
  };

  const renderModal = () => {
    if (modalState === MODAL_OPTIONS.SUCCESS) {
      return (
        <PokemonSuccessCatchModal
          errorMessage={errorMessage}
          handleSave={handleSave}
          nickname={nickname}
          setNickname={setNickname}
        />
      );
    }
    return (
      <PokemonFailCatchModal
        handleCancel={handleCloseModal}
        handleOk={handleRetryCatch}
      />
    );
  };
  
  return (
    <PokemonDetailWrapper>
      <ToastContainer />
      <div className="pokemon-detail--top">
        <div className="pokemon-detail__nav" onClick={back}>
          <img src={Back} alt="Back" /> 
          <b>Detail </b>
        </div>
        <div class="row">
          <div className="pokemon-detail__avatar">
            <img src={_.get(data, 'pokemon.sprites.front_default', '')} alt="Avatar" />
          </div>
          <div className="pokemon-detail__name">  
            <table>
              <tr className="pokemon-detail__name__title"><td> {capitalizeFirstLetter(name)} </td></tr>
              {isMine ?  
                <tr>
                  <th className="pokemon-detail__name__nickname"><b>Nickname : {nickname} </b></th>
                </tr>
                : null }  
            </table>
          </div>        
        </div>
      </div>
      <div className="pokemon-detail--bottom">
        {loading
          ? 'Loading...'
          : (
            <div className="pokemon-detail--bottom__content">
              <div className="pokemon-detail__content">
                <div className="pokemon-detail__content--height">
                  <img src={Height} alt="Height" /> 
                  <div className="pokemon-detail__data__label">Height</div>
                  <div className="pokemon-detail__data__value">
                    <b> {_.get(data, 'pokemon.height', 0)} Cm </b>
                  </div>
                </div>
                <div className="pokemon-detail__content--line"></div>
                <div className="pokemon-detail__content--weight">
                  <img src={Weight} alt="Weight" /> 
                  <div className="pokemon-detail__data__label">Weight</div>
                  <div className="pokemon-detail__data__value">
                    <b> {_.get(data, 'pokemon.weight', 0)} Gr </b>
                  </div>
                </div>
              </div>
              <hr className="pokemon-detail__data--horizontal" />
              <div className="pokemon-detail__data">
                <div className="pokemon-detail__data__label"><b>Types</b></div>
                <div className="pokemon-detail__data__types">
                  {
                    _.map(_.get(data, 'pokemon.types', []), (types, index) => (
                      <div
                        className="pokemon-detail__data__type"
                        key={`pokemon-detail__data__types-${index}`}
                      >
                        {capitalizeFirstLetter(types.type.name)}
                      </div>
                    ))
                  }
                </div>
              </div>
              <hr className="pokemon-detail__data--horizontal" />
              <div className="pokemon-detail__data">
                <div className="pokemon-detail__data__label"><b>Moves</b></div>
                <div className="pokemon-detail__data__moves">  
                  {
                    _.map(_.get(data, 'pokemon.moves', []), (moves, index) => (
                      <table>
                        <tr>
                          <td>
                            {capitalizeFirstLetter(moves.move.name)}, &nbsp;
                          </td>
                        </tr>
                      </table>  
                    ))
                  }
                </div>
              </div>

              {!loading && !isMine && (
                <div
                  aria-hidden="true"
                  className="pokemon-detail__button"
                  css={css`
                    animation: ${bounce} 1s ease infinite;
                  `}
                  onClick={handleCatchPokemon}
                >
                  <img src={CatchIcon} alt="Catch" />
                </div>
              )}
            </div>
          )}
      </div>

      {isModal && (
        <OverlayBackground>{renderModal()}</OverlayBackground>
      )}
    </PokemonDetailWrapper>
  );
};

PokemonDetail.propTypes = {
  isMine: PropTypes.bool,
};

PokemonDetail.defaultProps = {
  isMine: false,
};

export default PokemonDetail;