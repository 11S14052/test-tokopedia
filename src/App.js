import './App.css';
import PokemonDetail                                    from './pages/PokemonDetail';
import PokemonList                                      from './pages/PokemonList';
import React                                            from 'react';
import { BrowserRouter, Route, Switch }                 from 'react-router-dom';
import { ApolloClient, ApolloProvider, InMemoryCache }  from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache(),
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <PokemonList />
            </Route>
            <Route exact path="/list-pokemon">
              <PokemonList />
            </Route>
            <Route exact path="/list-pokemon/:name">
              <PokemonDetail />
            </Route>
            <Route exact path="/list-pokemon/my/:name/:nickname">
              <PokemonDetail isMine />
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
    </ApolloProvider>
  );
}

export default App;
